//- - - - - - - - - - - - - - - - - - - - - - - - 
//
// go outside, please
//
//- - - - - - - - - - - - - - - - - - - - - - - - 

var showingTip = false;

function ShowTip(tiptext = "text here")
{
	var ht = document.getElementById("hovertip");
	ht.style.display = 'block';
	ht.innerHTML = tiptext;
	showingTip = true;
}

function HideTip()
{
	var ht = document.getElementById("hovertip");
	ht.style.display = 'none';
	showingTip = false;
}

window.onmousemove = function (e) {
	if (!showingTip)
		return;
		
    var x = e.clientX, y = e.clientY;
    var ht = document.getElementById("hovertip");
    
    if (ht)
    {
		ht.style.left = (x+20) + 'px';
		ht.style.top = y + 'px';
	}
};

window.onload = function()
{
	// Assign tips to each button element
	const ul = document.querySelector('#headerblock ul');
	
	Array.from(ul.children).forEach((but) => {
		var tiptext = but.getAttribute("helper");
		
		but = but.firstChild;
		but.addEventListener('mouseenter', function() { ShowTip(tiptext); }, false);
		but.addEventListener('mouseleave', function() { HideTip(); }, false);
	});
}
